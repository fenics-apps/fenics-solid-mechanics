import ufl
import dolfin

from .fsm_cpp import plasticity_problem
from .fsm_cpp import plasticity_model
from fsm.quadrature import QuadratureFunction

#fsm_constitutive_update = ConstitutiveUpdate(u, Vs, J2)

class PlasticityProblem():

    def __init__(self, *args, **kwargs):
        """Initialize PlasticityProblem."""

        if len(args) != 7 and len(args) != 8:
            raise RuntimeError("Expecting 7 or 8 arguments, got ", len(args), ".")

        if not isinstance(args[0], ufl.Form):
            raise RuntimeError("Expecting a 'ufl Form' as argument 1, got ", type(args[0]), ".")
        self.a_ufl = args[0]
        self.a = dolfin.Form(self.a_ufl)
        if not isinstance(args[1], ufl.Form):
            raise RuntimeError("Expecting a 'ufl Form' as argument 2, got ", type(args[1]), ".")
        self.L_ufl = args[1]
        self.L = dolfin.Form(self.L_ufl)
        if not isinstance(args[2], dolfin.Function):
            raise RuntimeError("Expecting a 'dolfin Function' as argument 3, got ", type(args[2]), ".")
        self.u = args[2]
        if not isinstance(args[3], QuadratureFunction):
            raise RuntimeError("Expecting a 'QuadratureFunction' as argument 4, got ", type(args[3]), ".")
        self.tangent = args[3]
        if not isinstance(args[4], QuadratureFunction):
            raise RuntimeError("Expecting a 'QuadratureFunction' as argument 5, got ", type(args[4]), ".")
        self.sigma = args[4]
        if not isinstance(args[5], (list, tuple)):
            self.bcs = [args[5]]
        else:
            self.bcs = args[5]
        for bc in self.bcs:
            if not isinstance(bc, dolfin.cpp.fem.DirichletBC):
                raise RuntimeError("Unable to extract boundary condition arguments from argument 6")
        if not isinstance(args[6], plasticity_model.PlasticityModel):
            raise RuntimeError("Expecting a 'fsm_cpp PlasticityModel' as argument 7, got ", type(args[6]), ".")
        self.plasticity_model = args[6]
        if len(args) == 7:
            self._cpp_object = plasticity_problem.PlasticityProblem(self.a, self.L, self.u.cpp_object(), 
                                                                    self.tangent.cpp_object(), self.sigma.cpp_object(), 
                                                                    self.bcs, self.plasticity_model)
        else:
            if not isinstance(args[7], dolfin.NewtonSolver):
                raise RuntimeError("Expecting a 'dolfin NewtonSolver' as argument 7, got ", type(args[7]), ".")
            self.solver = args[7]
            self._cpp_object = plasticity_problem.PlasticityProblem(self.a, self.L, self.u.cpp_object(), 
                                                                    self.tangent.cpp_object(), self.sigma.cpp_object(), 
                                                                    self.bcs, self.plasticity_model, self.solver)
    def cpp_object(self):
        return self._cpp_object
